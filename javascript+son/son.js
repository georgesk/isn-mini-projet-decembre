/**
 * programme pour jouer un son inclus dans la page avec une balise audio
 *
 * Copyright: (c) 2018 Georges Khaznadar <georgesk@debian.org>
 * Licence GNU GPL V3+
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

/**
 * Joue un son en activant un des éléments audio de la page.
 * Afin de pouvoir jouer d'autres sons tout de suite, cette fonction
 * n'est pas bloquante : elle ajoute une action à faire "en parallèle",
 * tout de suite, pour activer le son.
 * @param nomFichier nom d'un fichier, qui doit se retrouver comme attribut
 *  src d'un des éléments audio de la page.
 **/
function joue(nomFichier){
    var audios=document.querySelectorAll("audio");
    var ok = false;
    for(var i=0; i < audios.length; i++){
	var audio=audios[i];
	if (nomFichier == audio.getAttribute("src")){
	    ok=true;
	    /* joue le son puisqu'on a trouvé le bon élément dans la page */
	    /* le son est joué dans un processus parallèle, lancé aussitôt */
	    setTimeout(activeCetAudio(audio), 0);
	}
    }
    /* au cas où on ne trouve pas le bon élément audio, on râle un peu */
    if (ok==false) alert("Le son "+nomFichier+" n'a pas été trouvé");
}

/**
 * fabrique une fonction qui active un élément audio, tel qu'il est
 * fourni au moment de l'appel ; activeCetAudio est une fabrique de fonction
 * @param audio un élément <audio>
 * @return une fonction qui démarre la lecture de cet audio (s'il ne joue pas)
 **/
function activeCetAudio(audio){
    return function (){audio.play()};
}

/**
 * bascule la ligne du tableau dont l'id est "lecteurs"
 * en mode visible/invisible
 **/
function montrecache(){
    var ligne=document.querySelector("#lecteurs");
    if (ligne.getAttribute("class")=="visible"){
	ligne.setAttribute("class","invisible");
    } else {
	ligne.setAttribute("class","visible");
    }
}
