## Projets ISN du bac 2019 ##

pour les élèves :

  * créer un compte à https://framagit.org/users/sign_in
  * ne pas oublier la paire login/passe !
  * me communiquer le login, et faire une demande d'inscription au projet https://framagit.org/georgesk/isn-mini-projet-decembre

Quand les droits et les permissions sont OK, le dépôt Git commun est utilisable.

### Règles à respecter : ###

  * chaque groupe travaille dans un sous-répertoire distinct
  * les membres d'un groupe n'effacent pas les travaux d'un autre groupe
  * l'entraide est bienvenue.
  * ne pas hésiter à définir des tags pour marquer les "moments importants" : chaque groupe choisit un préfixe facile à reconnaître pour "ses" tags.
