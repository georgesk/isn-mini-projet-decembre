/**
 * programme pour calculer la différence de deux images
 *
 * Copyright: (c) 2018 Georges Khaznadar <georgesk@debian.org>
 * Licence GNU GPL V3+
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

/* le programme suppose que toutes les images ont une dimension 400 x 400 */
var w=400,h=400;

/* 0, 1 ,2 , 3 : pour rouge, vert, bleu, opacité, dans un tableau imageData */
var r=0, v=1, b=2, a=3;

/**
 * Cette fonction compare deux images de taille w x h
 * et injecte dans un canvas de la même taille une image qui se base sur
 * le différences détectées entre les images, pixel par pixel.
 * @param id1 l'attribut id d'un élément img
 * @param id2 l'attribut id d'un élément img
 * @param id3 l'attribut id d'un élément canvas
 **/
function difference(id1, id2, id3){
    var i1=document.querySelector("#"+id1);
    var i2=document.querySelector("#"+id2);
    var c= document.querySelector("#"+id3);
    var ctx=c.getContext('2d')
    var imd=ctx.getImageData(0, 0, w, h);
    
    var data1=imgToData(i1);
    var data2=imgToData(i2);

    /* parcours des images ligne par ligne, colonne par colonne */
    for (ligne=0; ligne < h; ligne++){
	for(colonne=0; colonne < w; colonne++){
	    /* index dans le tableau des couleurs, en mode rvba */
	    var i=4*(ligne*w+colonne);
	    /* inscrit un pixel dans le canvas */
	    diffPixel(i, data1, data2, imd.data);
	}
    }
    /* on renvoie le tableau de données modifiées dans le canvas */
    ctx.putImageData(imd,0,0);

}

/**
 * fabrique un tableau imageData à partir d'un élément img
 * @param im un élément IMG
 * @return un tableau de valeurs rgba construit à partir de l'image
 **/
function imgToData(im){
    /* on crée un canvas non enraciné dans le document */
    var c=document.createElement("canvas");
    c.width=w; c.height=h;
    /* on récupère son contexte à deux dimensions */
    var ctx=c.getContext('2d');
    /* on peint l'image sur le canvas */
    ctx.drawImage(im, 0, 0, w, h);
    /* on exporte le tableau imageData depuis le contexte graphique 2D */
    var d=ctx.getImageData( 0, 0, w, h).data;
    return d;
}


/**
 * calcule une "distance" entre deux couleurs RVB, et en fait une autre couleur,
 * pour un pixel
 * @param i un index dans un tableau imageData (forcément multiple de 4)
 * @param data1 un tableau imageData
 * @param data2 un tableau imageData
 * @param resultat un tableau imageData, qui sera modifié. Unique contrainte,
 *  les 4 octets commençant à index i sont entre 0 et 255 (effectivement, dans
 *  le cas contraire, ça ne donne plus une quantité représentable par un
 *  octet ; le résultat sera fautif, mais rien de prévisible, cela dépend
 *  de l'implémentation du moteur javascript)
 **/
function diffPixel(i,data1, data2, resultat){
    /* on utilise une simple valeur absolue pour les couleurs */
    resultat[i+r] = Math.abs(data1[i+r]-data2[i+r]);
    resultat[i+v] = Math.abs(data1[i+v]-data2[i+v]);
    resultat[i+b] = Math.abs(data1[i+b]-data2[i+b]);
    /* l'opacité est mise au maximum dans tous les cas */
    resultat[i+a] = 255;
}
