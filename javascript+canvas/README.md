Traitement d'images en Javascript, à l'aide de Canvas
=====================================================

Un élément de type Canvas permet d'accéder aux pixels d'une image
dans un tableau à un seul index. Chaque pixel est codé sur quatre octets.
Les quatre octets d'un pixel sont :
  1. la valeur de rouge (0 à 255)
  2. la valeur de vert (0 à 255)
  3. la valeur de bleu (0 à 255)
  4. l'opacité (0 à 255)
  
Le début du code d'un pixel dans le tableau du canvas est à :
  * `décalage = 4 * (largeur * numeroDeLigne + numeroDeColonne)`
  * `numeroDeLigne` est la position verticale du pixel à partir du haut
  * `numeroDeColonne` est la position horizontale du pixel à partir de la gauche
  
Ce répertoire contient un exemple d'une page HTML (index.html) qui inclut
deux images (i1.png, i2.png), et qui calcule puis affiche la différence
entre les deux images. L'opacité est supposée maximale partout.

On est en ISN
